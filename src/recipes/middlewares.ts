import multer from "multer"

// const uploads = multer({dest: "uploads"})
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      const toks = file.originalname.split('.')
      const extension = toks[toks.length-1]
      cb(null, uniqueSuffix+"."+extension)
    }
  })
  
export const uploads = multer({ storage: storage })
