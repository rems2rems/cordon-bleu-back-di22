import { RequestHandler } from "express";
import { DbRecipe, NewRecipe } from "./models";
import { randomUUID } from "crypto";

const recipes : DbRecipe[] = []

export const addRecipe : RequestHandler = (req, res) => {
    const newRecipe = req.body as NewRecipe
    console.log("file",req.file);
    
    newRecipe.image = req.file?.path
    console.log("new recipe", newRecipe)
    //////////////////
    //simule une bdd
    recipes.push({id:randomUUID(),...newRecipe})
    //////////////////
    res.send()
}

export const listRecipes : RequestHandler = (req, res) => {
    console.log("nom cookie",req.cookies.nom);
    
    res.status(200).json(recipes)
}