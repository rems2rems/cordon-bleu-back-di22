
export type NewRecipe = {name: string,image: string | undefined}
export type DbRecipe = {id: string,name: string,image: string | undefined}
