import cookieParser from "cookie-parser"
import express from 'express'
import { login, logout, refreshTokens, signup } from "./auth/controllers"
import { User } from "./auth/models"
import { checkAuthJwt, checkAuthRefreshJwt } from "./auth/middlewares"
import { uploads } from "./recipes/middlewares"
import { addRecipe, listRecipes } from "./recipes/controllers"
import cors from "cors"
import rateLimit from "express-rate-limit"
import { StatusCodes } from "http-status-codes"

declare module 'express-session' {
    interface SessionData {
        user: User
    }
}

const app = express()
app.use(cors({origin:"https://cordon-bleu-front.loca.lt"}))
app.use(cookieParser(process.env.COOKIE_SECRET))

app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(express.static("public"))
app.use("/uploads",express.static("uploads"))

app.use(rateLimit({windowMs: 1*60*1000, limit: 5,statusCode:StatusCodes.TOO_MANY_REQUESTS}))

// app.use(session({
//     secret: process.env.COOKIE_SECRET!,
//     resave: false,
//     saveUninitialized: true,
// }))

app.use((req, res, next) => {
    console.log("request:", req.method, req.path);
    next()
    console.log("request ended with status:", res.statusCode);
})

app.get('/', (req, res) => {
    res.status(200).send("hello world!")
})

app.post('/recipes',checkAuthJwt,uploads.single("image"), addRecipe)
app.get("/recipes",checkAuthJwt, listRecipes)

app.post('/auth/signup', signup)
app.post('/auth/login', login)
app.post("/auth/refresh",checkAuthRefreshJwt,refreshTokens)
app.all("/auth/logout",checkAuthJwt,logout)

const port = parseInt(process.env.PORT || "8080")
app.listen(port,()=>{
    console.log(`listening on port ${port}...`)
})