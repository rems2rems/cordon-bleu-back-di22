import { compare, hash } from "bcrypt"
import { RequestHandler } from 'express'
import { StatusCodes } from "http-status-codes"
import { sign, verify } from "jsonwebtoken"
import { randomUUID } from "node:crypto"
import { blacklist } from "./middlewares"
import { DbUser, LoginData, SignupData } from "./models"

export const users : DbUser[] = []

export const signup : RequestHandler = (req, res) => {
    console.log("sign up")
    const bodyUser = req.body as SignupData
    console.log("body",bodyUser);
    
    if(bodyUser.password !== bodyUser.confirmPassword){
        res.status(StatusCodes.BAD_REQUEST).send("passwords do not match")
        return
    }
    hash(bodyUser.password, 10, (err : any, hash : string) => {
        const newUser  : DbUser = {id : randomUUID(),name: bodyUser.name, pwdHash: hash}
        users.push(newUser)
        res.status(StatusCodes.CREATED).send()
    })
}
export const login : RequestHandler = (req, res) => {
    console.log("login:",users)
    const bodyUser = req.body as LoginData
    const user = users.find(user => user.name === bodyUser.name)
    if(!user){
        res.status(StatusCodes.UNAUTHORIZED).send("auth pb")
        return
    }
    compare(bodyUser.password, user.pwdHash, (err : any, pwdMatch : boolean) => {
        if(!pwdMatch){
            res.status(StatusCodes.UNAUTHORIZED).send("auth pb")
            return
        }

        //methode 1 : session (serveur)
        // req.session.user = user
        
        //methode 2 : token envoyé au client
        const data = {user:{id:user.id,name:user.name},fingerprint:req.headers["sec-ch-ua"]}
        console.log("data",data);
        
        const token = sign(data,process.env.JWT_SECRET!,{expiresIn: "1min"})
        res.cookie("at",token,{httpOnly: true})
        
        res.cookie("rt",sign({name:user.name},process.env.JWT_SECRET!,{expiresIn: "90d"}),{httpOnly: true})
        res.status(StatusCodes.OK).send()
    })
}

export const logout : RequestHandler = (req, res) => {
    if(verify(req.cookies.at,process.env.JWT_SECRET!)){ 
        blacklist.push(req.cookies.at)
    }
    res.clearCookie("at")
    res.clearCookie("rt")
    res.send()
}

export const refreshTokens : RequestHandler = (req : any, res) => {
    console.log("refresh tokens...");
    
    const user = users.find(user => user.name === req.refreshToken.name)
    if(!user){
        res.status(StatusCodes.UNAUTHORIZED).send("auth pb")
        return
    }   
        
    const data = {user:{id:user.id,name:user.name},fingerprint:req.headers["sec-ch-ua"]}
    console.log("data",data);
    
    const token = sign(data,process.env.JWT_SECRET!,{expiresIn: "5min"})
    res.cookie("at",token)
    res.cookie("rt",sign({name:user.name},process.env.JWT_SECRET!,{expiresIn: "90d"}))
    res.status(StatusCodes.OK).send()
}