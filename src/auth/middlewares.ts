import { StatusCodes } from "http-status-codes"
import express, { NextFunction, RequestHandler } from 'express'
import { verify } from "jsonwebtoken"

export const blacklist : string[] = []


export const checkAuthSession : RequestHandler = (req, res, next : NextFunction) => {
    if(!req.session.user){
        res.status(StatusCodes.UNAUTHORIZED).send()
        return
    }
    next()
}

export const checkAuthJwt : RequestHandler = (req, res, next : express.NextFunction) => {
    console.log("cookies:",req.cookies);
    const token = req.cookies.at
    if(!token){
        res.status(StatusCodes.UNAUTHORIZED).send()
        return
    }
    try {
        const data : any = verify(token,process.env.JWT_SECRET!)
        
        if(blacklist.includes(token)){
            res.status(StatusCodes.UNAUTHORIZED).send()
            return
        }
        console.log(data.fingerprint,req.headers["sec-ch-ua"]);
        
        if(data.fingerprint !== req.headers["sec-ch-ua"]){
            res.status(StatusCodes.UNAUTHORIZED).send()
            return
        }
    } catch (error) {
        res.status(StatusCodes.UNAUTHORIZED).send()
        return
    }
    
    next()
}

export const checkAuthRefreshJwt : RequestHandler = (req , res, next : express.NextFunction) => {
    console.log("cookies:",req.cookies);
    const refreshToken : string = req.cookies.rt!
    if(!refreshToken){
        console.log("no refresh token");
        res.status(StatusCodes.UNAUTHORIZED).send()
        return
    }
    try {
        const data : any = verify(refreshToken,process.env.JWT_SECRET!);
        (req as any).refreshToken = data
        next()
    } catch (error) {
        console.log("invalid token");
        res.status(StatusCodes.UNAUTHORIZED).send()
        return
    }
}