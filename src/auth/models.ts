
export type SignupData = {name:string,password: string,confirmPassword: string}
export type LoginData = {id: string,name: string,password: string}
export type DbUser = {id: string,name: string,pwdHash: string}
export type User = {id: string,name: string}